<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    //
    protected $table = 'document_ro';
    protected $fillable = ['task_ro_id', 'name'];

    public $timestamps = false;
}
