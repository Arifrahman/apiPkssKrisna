<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $table = 'task_ro';
    protected $fillable = ['task_type_id', 'user_id', 'client_id', 'catatan', 'signature', 'penerima', 'jabatan', 'invoice_id', 'status', 'for_date'];

    public function types() {
    	return $this->belongsTo('App\TaskType', 'task_type_id', 'id');
    }
}
