<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use File;
use Hash;
use App\User;
use App\TaskType;
use App\Task;
use App\Document;

class ApiController extends Controller
{
    //

    public function doLogin(Request $request) {
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email', 
            'password' => 'required'
        ]);

        if ($validator->fails()) {
        	# code...
        	return response(['status' => false, 'message' => 'Email atau Password Salah'], 401);
        } else {
        	$ifEmailisExists = User::where('email', $request->email)->get();
        	if (count($ifEmailisExists) <= 0) {
        		# code...
        		return response(['status' => false, 'message' => 'Email Tidak Terdaftar'], 401);
        	} else {
        		if ( Auth::attempt(['email' => $request->email, 'password' => $request->password]) ) {
        			# code...
        			$user = Auth::user();
        			$token = $user->createToken('RO TOKEN')->accessToken;
        			return response(['status' => true, 'message' => 'Login Berhasil', 'data' => $token], 200);
        		} else {
        			return response(['status' => false, 'message' => 'Email atau Password Salah'], 401);
        		}
        	}
        }
    }

    public function getUserData() {
    	$user = Auth::user();
    	return response(['status' => true, 'message' => 'User Ditemukan', 'data' => $user], 200);
    }

    public function doChangePassword(Request $request) {
    	$valid = Validator::make($request->all(), [
    		'old_password' => 'required',
    		'new_password' => 'required',
    		'confirm_password' => 'required_with:new_password|same:new_password'
    	]);

    	if ($valid->fails()) {
    		# code...
    		return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 401);
    	} else {
    		if (Hash::check($request->old_password, Auth::user()->password)) {
    			# code...
    			$update = User::where('id', Auth::user()->id)->update([
    				'password' => bcrypt($request->confirm_password)
    			]);

    			if ($update) {
    				# code...
    				return response(['status' => true, 'message' => 'Password Berhasil Diubah'], 200);
    			} else {
    				return response(['status' => false, 'message' => 'Gagal Ubah Password'], 401);
    			}
    		} else {
    			return response(['status' => false, 'message' => 'Password Lama Salah'], 401);
    		}
    	}
    }

    public function doUpdateProfile(Request $request) {
    	$valid = Validator::make($request->all(), [
    		'name' => 'required',
    		'nik' => 'required',
    		'email' => 'required',
    		'birthdate' => 'required',
    		'phone' => 'required',
    		'image' => 'image:mimes:jpg,jpeg,png'
    	]);

    	if ($valid->fails()) {
    		# code...
    		return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 401);
    	} else {
    		if ($request->hasFile('image')) {
    			# code...
    			$user = User::where('id', Auth::user()->id)->update([
	    			'name' => $request->name,
	    			'nik' => $request->nik,
	    			'email' => $request->email,
	    			'birthdate' => $request->birthdate,
	    			'phone' => $request->phone,
	    			'image' => $this->savePhoto($request->file('image'))
	    		]);

	    		if ($user) {
	    			# code...
	    			return response(['status' => true, 'message' => 'Profile Berhasil Diupdate'], 200);
	    		} else {
	    			return response(['status' => false, 'message' => 'Gagal Update Profile'], 401);
	    		}
    		} else {
    			$cek = User::where('id', Auth::user()->id)->first();
    			$user = User::where('id', Auth::user()->id)->update([
	    			'name' => $request->name,
	    			'nik' => $request->nik,
	    			'email' => $request->email,
	    			'birthdate' => $request->birthdate,
	    			'phone' => $request->phone,
	    			'image' => $cek['image']
	    		]);

	    		if ($user) {
	    			# code...
	    			return response(['status' => true, 'message' => 'Profile Berhasil Diupdate'], 200);
	    		} else {
	    			return response(['status' => false, 'message' => 'Gagal Update Profile'], 401);
	    		}
    		}
    	}
    }

    public function getTaskList() {
    	$task = Task::where('user_id', Auth::user()->id)->groupBy('for_date')->get();
    	if (count($task) <= 0) {
    		# code...
    		return response(['status' => false, 'message' => 'Task Kosong', 'data' => []], 401);
    	} else {
    		return response(['status' => true, 'message' => 'Task Ditemukan', 'data' => $task], 200);
    	}
    }

    public function getFilterTaskByDate(Request $request) {
    	$valid = Validator::make($request->all(), [
    		'date' => 'required'
    	]);

    	if ($valid->fails()) {
    		# code...
    		return response(['status' => false, 'message' => 'Tanggal Masih Kosong'], 401);
    	} else {
    		$task = Task::where('user_id', Auth::user()->id)->whereDate('for_date', $request->date)->groupBy('for_date')->get();
	    	if (count($task) <= 0) {
	    		# code...
	    		return response(['status' => false, 'message' => 'Task Kosong', 'data' => []], 401);
	    	} else {
	    		return response(['status' => true, 'message' => 'Task Ditemukan', 'data' => $task], 200);
	    	}
    	}
    }

    public function getTaskDetail(Request $request) {
    	$valid = Validator::make($request->all(), [
    		'id' => 'required'
    	]);

    	if ($valid->fails()) {
    		# code...
    		return response(['status' => false, 'message' => 'Task Belum Dipilih'], 401);
    	} else {
    		$task = Task::where('id', $request->id)->first();
    		if (!$task) {
    			# code...
    			return response(['status' => false, 'message' => 'Task Kosong', 'data' => []], 401);
    		} else {
    			return response(['status' => true, 'message' => 'Task Ditemukan', 'data' => $task], 200);
    		}
    	}
    }

    public function acceptTask(Request $request) {
    	$valid = Validator::make($request->all(), [
    		'notes' => 'required',
    		'penerima' => 'required',
    		'jabatan' => 'required',
    		'signature' => 'image|mimes:jpg,jpeg,png',
    		'document' => 'image|mimes:jpg,jpeg,png'
    	]);

    	if ($valid->fails()) {
    		# code...
    		return response(['status' => false, 'message' => 'Data Tidak Lengkap'], 401);
    	} else {
    		if ($request->hasFile('signature') && $request->hasFile('document')) {
    			# code...
    			$create = Task::create([
    				'catatan' => $request->notes,
    				'penerima' => $request->penerima,
    				'jabatan' => $request->jabatan,
    				'signature' => $this->saveSignature($request->file('signature')),
    				'status' => 'Y'
    			]);

    			if (is_array($request->document)) {
    				# code...
    				for ($i=0; $i < count($request->document); $i++) { 
    					# code...
    					Document::create([
    						'task_ro_id' => $create->id,
    						'name' => $this->saveDocument($request->file('document')[$i])
    					]);
    				}
    			} else {
					Document::create([
						'task_ro_id' => $create->id,
						'name' => $this->saveDocument($request->file('document'))
					]);
    			}

    			if ($create) {
    				# code...
    				return response(['status' => true, 'message' => 'Acc Tugas Berhasil'], 200);
    			} else {
    				return response(['status' => false, 'message' => 'Gagal Acc Tugas'], 401);
    			}
    		} else {
    			return response(['status' => false, 'message' => 'Document atau Signature Kosong'], 401);
    		}
    	}
    }

    public function doAddTask(Request $request) {
    	$valid = Validator::make($request->all(), [
    		'task_type_id' => 'required',
    		'client' => 'required',
    		'penerima' => 'required',
    		'notes' => 'required',
    		'jabatan' => 'required',
    		'client_id' => 'required',
    		'signature' => 'image|mimes:jpg,jpeg,png',
    		'document' => 'image|mimes:jpg,jpeg,png'
    	]);

    	if ($valid->fails()) {
    		# code...
    		return response(['status' => false, 'message' => 'Data Tidak Valid'], 401);
    	}else {
    		if ($request->hasFile('document') && $request->hasFile('signature')) {
    			# code...
    			$create = Task::create([
    				'task_type_id' => $request->task_type_id,
    				'user_id' => Auth::user()->id,
    				'client_id' => $request->client,
    				'catatan' => $request->notes,
    				'signature' => $request->signature,
    				'penerima' => $request->penerima,
    				'jabatan' => $request->jabatan,
    				'invoice_id' => null,
    				'status' => 'Y',
    				'for_date' => date('Y-m-d')
    			]);

    			if (is_array($request->document)) {
    				# code...
    				for ($i=0; $i < count($request->document); $i++) { 
    					# code...
    					Document::create([
    						'task_ro_id' => $create->id,
    						'name' => $this->saveDocument($request->file('document')[$i])
    					]);
    				}
    			} else {
					Document::create([
						'task_ro_id' => $create->id,
						'name' => $this->saveDocument($request->file('document'))
					]);
    			}

    			if ($create) {
    				# code...
    				return response(['status' => true, 'message' => 'Tugas Berhasil Ditambahkan'], 200);
    			} else {
    				return response(['status' => false, 'message' => 'Gagal Menambahkan Tugas'], 401);
    			}

    		} else {
    			return response(['status' => false, 'message' => 'Anda Belum Tanda Tangan'], 401);
    		}
    	}
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'user';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['image'];
    }

    protected function saveDocument($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'document';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['document'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['document'];
    }

    protected function saveSignature($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'signature';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['image'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['image'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
	}
	

	protected function getReport(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'fromDate' => 'required', 
			'toDate' => 'required'
		]);
		
		if($validator->fails()) {
        	# code...
        	return response(['status' => false, 'message' => 'Format request tidak lengkap.'], 401);
		} else {
			$transaksi = DB::table('task_ro')
				->where('user_id', '=', Auth::user()->id)
				->whereBetween('for_date', [$request->fromDate, $request->toDate])
				->get();
			
			if(count($transaksi) <= "0") {
				return response(['status' => false, 'message' => 'Data tidak ditemukan'], 401);

			} else {
				return response(['status' => true, 'message' => 'Data Ditemukan', 'data' => $transaksi], 200);
			}

		}
	}

	protected function dashboard(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'date' => 'required'
		]);
		
		if($validator->fails()) {
        	# code...
        	return response(['status' => false, 'message' => 'Format request tidak lengkap.'], 401);
		} else {
			$selesai = DB::table('task_ro')
				->select('status')
				->where([ ['user_id', '=', Auth::user()->id],
							['for_date', $request->date],
							['status', 'Y']
						])
				->count();
			
			$pending = DB::table('task_ro')
				->select('status')
				->where([ ['user_id', '=', Auth::user()->id],
							['for_date', $request->date],
							['status', 'N']
						])
				->count();

			$nominalPending = DB::table('task_ro')
				->join('invoices','task_ro.invoice_id', '=', 'invoices.id')
				->where([ ['task_ro.user_id', '=', Auth::user()->id],
							['task_ro.for_date', $request->date],
							['task_ro.status', 'N']
						])
				->sum('invoices.amount');
			
			$nominalSelesai = DB::table('task_ro')
				->join('invoices','task_ro.invoice_id', '=', 'invoices.id')
				->where([ ['task_ro.user_id', '=', Auth::user()->id],
							['task_ro.for_date', $request->date],
							['task_ro.status', 'Y']
						])
				->sum('invoices.amount');
			
			return response(['status' => true, 'message' => 'Data Ditemukan', 'totalSelesai' => $selesai, 'totalPending' => $pending, 'nominalPending' =>$nominalPending, 'nominalSelesai' =>$nominalSelesai ], 200);
			

		}
	}
}
