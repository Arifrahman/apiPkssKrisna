<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['guest', 'api'], 'prefix' => 'v1'], function () {
	Route::post('auth/login', 'ApiController@doLogin');
});

Route::group(['middleware' => ['auth:api'], 'prefix' => 'v1'], function () {
	Route::get('auth/user', 'ApiController@getUserData');
	Route::post('auth/reset-password', 'ApiController@doChangePassword');
	Route::post('auth/edit-profile', 'ApiController@doUpdateProfile');
	Route::get('task/list', 'ApiController@getTaskList');
	Route::post('task/filter/date', 'ApiController@getFilterTaskByDate');
	Route::post('task/detail', 'ApiController@getTaskDetail');
	Route::post('task/accept-task', 'ApiController@acceptTask');
	Route::post('task/store', 'ApiController@doAddTask');

	Route::post('task/reporting', 'ApiController@getReport');
	Route::post('task/dashboard', 'ApiController@dashboard');

});